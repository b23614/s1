//Main class
    //entry point for our java program
    //main class has 1 method inside, the "main method" => run code

    // public - access modifier which simple tells the application which classes have access to method / attributes.
    // static - keyword associated with a method/property that is related in a class. This will allow a method to be invoked without instantiating a class.
    // void - a keyword that is used to specify a method that doesn't return anything. In java we have to declare the data type of the method's return.
    // main() - is the name of the main method in Java. It is entry point for the java program.
    // String[] args - accepts a single argument of type String array that contains command line argument.
    // command line arguments is use to affect the operation of the program, or to pass information to the program, at runtime.
public class Main {
    public static void main(String[] args) {
    // a statement that allows us to print the value of arguments passed into its terminal.
        System.out.println("Hello world!");
    }
}