package com.zuitt.example;

import java.util.Scanner;

public class S1A1 {
    public static void main(String[] args){
        Scanner userInput = new Scanner(System.in);

        System.out.println("First Name:");
        String firstName = userInput.nextLine();

        System.out.println("Last Name:");
        String lastName = userInput.nextLine();

        System.out.println("First Subject Grade:");
        double firstSubject = userInput.nextDouble();

        System.out.println("Second Subject Grade:");
        double secondSubject = userInput.nextDouble();

        System.out.println("Third Subject Grade:");
        double thirdSubject = userInput.nextDouble();

        System.out.println("Good day, "+ firstName+ " " + lastName + ".");

        double averageScore = (firstSubject + secondSubject + thirdSubject) / 3;
        int i = (int)averageScore;
        System.out.println("Your grade average is: " + i);
    }
}
